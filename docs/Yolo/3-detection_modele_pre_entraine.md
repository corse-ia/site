# Détection avec le modèle pré-entrainé

<figure markdown>
  ![Image Yolo détection](https://user-images.githubusercontent.com/26833433/243418624-5785cb93-74c9-4541-9179-d5c6782d491a.png){ width="900" }
  <figcaption>Figure 1 : Détection d'objets</figcaption>
</figure>

^^**Documentations à consulter :**^^

[Quickstart](https://docs.ultralytics.com/fr/quickstart/){ target="_blank" .md-button }  [Predict](https://docs.ultralytics.com/fr/modes/predict/){ target="_blank" .md-button }  [Python Utilisation](https://docs.ultralytics.com/fr/usage/python/){ target="_blank" .md-button }


## Programmes de tests

Les programmes ci-dessous permettent de tester rapidement la détection d'objets avec le [modèle Yolo pré entrainé](https://docs.ultralytics.com/fr/tasks/detect/#models){ target="_blank" } `yolov8n.pt`.

Dans ces programmes la ligne `results = model(frame)` lance une prédiction et renvoie une liste d'objets [`ultralytics.engine.results`](https://docs.ultralytics.com/fr/reference/engine/results/){ target="_blank" }. Ces objets possèdent plusieurs attributs interessants : [**`Boxes`**](https://docs.ultralytics.com/fr/reference/engine/results/#ultralytics.engine.results.Boxes){ target="_blank" } (contours des objets détectés), [`masks`](https://docs.ultralytics.com/fr/modes/predict/#masks){ target="_blank" },  [`keypoints`](https://docs.ultralytics.com/fr/modes/predict/#keypoints){ target="_blank" },  [**`names`**](https://docs.ultralytics.com/fr/reference/engine/results/#ultralytics.engine.results.Boxes){ target="_blank" } (noms des classes).

L'objet renvoyé [`ultralytics.engine.results.Boxes`](https://docs.ultralytics.com/fr/reference/engine/results/#ultralytics.engine.results.Boxes){ target="_blank" } est le plus interessant car il contient plusieurs attributs dont :

- `cls` : Les classes des objets détectés.
- `conf` : Les niveau de confiance des objets détectés.
- Les coordonnées des boites sous différents formats (`xywh`, `xywhn`, `xyxy`, `xyxyn`).


=== "Webcam"

    ``` python
    import cv2
    from ultralytics import YOLO


    # Load a model
    model = YOLO('yolov8n.pt')  # pretrained YOLOv8n model

    # Open the webcam source
    source = "/dev/video0"

    cap = cv2.VideoCapture(source)

    # Loop through the video frames
    while cap.isOpened():
        # Read a frame from the video
        success, frame = cap.read()

        if success:
            # Run YOLOv8 inference on the frame
            results = model(frame, stream=False)

            # Visualize the results on the frame
            annotated_frame = results[0].plot()

            # Display the annotated frame
            cv2.imshow("YOLOv8 Inference", annotated_frame)

            # Break the loop if 'q' is pressed
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

    # Release the video capture object and close the display window
    cap.release()
    cv2.destroyAllWindows()
    ```

=== "Fichier vidéo"

    ``` python
    import cv2
    from ultralytics import YOLO


    VIDEO_PATH = "chemin_vers_le_fichier_video.mp4"
    OUTPUT_VIDEO_PATH = "output.mp4"

    #model = YOLO('yolov8n.pt')  # pretrained YOLOv8n model

    cap = cv2.VideoCapture(VIDEO_PATH)
    assert cap.isOpened(), "Error reading video file"

    # Loop through the video frames
    while cap.isOpened():
        success, frame = cap.read()   # Read a frame from the video

        if success:
            results = model(frame, stream=False, verbose=True)  # Run YOLOv8 inference on the frame
            annotated_frame = results[0].plot()  # Visualize the results on the frame

            cv2.imshow("YOLOv8 Inference", annotated_frame)   # Display the annotated frame

            if cv2.waitKey(1) & 0xFF == ord("q"):  # Break the loop if 'q' is pressed
                break
        else:
            break  # Break the loop if the end of the video is reached

    cap.release()
    cv2.destroyAllWindows()
    ```

=== "Fichier vidéo avec enregistrement"

    ``` python
    import cv2
    from ultralytics import YOLO



    VIDEO_PATH = "chemin_vers_le_fichier_video.mp4"
    OUTPUT_VIDEO_PATH = "output.mp4"

    model = YOLO('yolov8n.pt')  # pretrained YOLOv8n model

    cap = cv2.VideoCapture(VIDEO_PATH)
    assert cap.isOpened(), "Error reading video file"

    w, h, fps = (int(cap.get(x)) for x in (cv2.CAP_PROP_FRAME_WIDTH, cv2.CAP_PROP_FRAME_HEIGHT, cv2.CAP_PROP_FPS))
    video_writer = cv2.VideoWriter(OUTPUT_VIDEO_PATH, cv2.VideoWriter_fourcc(*'mp4v'), fps, (w, h))

    # Loop through the video frames
    while cap.isOpened():
        success, frame = cap.read()  # Read a frame from the video

        if success:
            results = model(frame, stream=False, verbose=True)    # Run YOLOv8 inference on the frame
            annotated_frame = results[0].plot()   # Visualize the results on the frame

            cv2.imshow("YOLOv8 Inference", annotated_frame)   # Display the annotated frame
            video_writer.write(annotated_frame)   # Write the annotated frame

            if cv2.waitKey(1) & 0xFF == ord("q"):  # Break the loop if 'q' is pressed
                break
        else:
            break   # Break the loop if the end of the video is reached

    cap.release()
    video_writer.release()
    cv2.destroyAllWindows()
    ``` 


=== "Comptage d'objets"

    Ce programme utilise la classes [`ultralytics.solutions.object_counter.ObjectCounter`](https://docs.ultralytics.com/fr/reference/solutions/object_counter/#ultralytics.solutions.object_counter.ObjectCounter){ target="_blank" }. Les décomptes sont dans les attributs `in_counts` et `out_counts`.

    ``` python
    from ultralytics import YOLO
    from ultralytics.solutions import object_counter
    import cv2



    SOURCE = "/dev/video0"  # La webcam
    
    model = YOLO("yolov8n.pt")


    cap = cv2.VideoCapture(SOURCE)

    assert cap.isOpened(), "Error reading video file"

    # Define region points (rectangle, ligne, etc)
    #region_points = [(250, 10), (350, 10), (350, 470), (250, 470)]
    line_points = [(320, 5), (320, 475)]
    classes_to_count = [0, 2]  # person and car classes for count

    # Init Object Counter
    counter = object_counter.ObjectCounter()
    counter.set_args(view_img=True,  # Affiche la source vidéo
                    view_in_counts=True,  # Affiche le compteur d'objets in
                    view_out_counts=True,   # Affiche le compteur d'objets out
                    reg_pts=line_points,
                    classes_names=model.names,  # Classes à décompter
                    draw_tracks=True,  # Affiche les trajectoires
                    )

    while cap.isOpened():
        success, im0 = cap.read()
        if success:
            tracks = model.track(im0, conf=0.6, persist=True, show=False,  verbose=True, classes=classes_to_count, )

            im0 = counter.start_counting(im0, tracks)  # Renvoie l'image données en paramètre si un objet est détecté
            try:
                cv2.imshow("YOLOv8 Inference", im0)
            except Exception as e:
                print(e)
            
            #print('COUNTERS (in/out:', counter.in_counts, counter.out_counts)

    cap.release()
    cv2.destroyAllWindows()
    ```

=== "Heatmap"

    ``` python
    from ultralytics import YOLO
    from ultralytics.solutions import heatmap
    import cv2


    model = YOLO("yolov8n.pt")
    source = "/dev/video0"

    cap = cv2.VideoCapture(source)
    assert cap.isOpened(), "Error reading video file"
    w, h, fps = (int(cap.get(x)) for x in (cv2.CAP_PROP_FRAME_WIDTH, cv2.CAP_PROP_FRAME_HEIGHT, cv2.CAP_PROP_FPS))

    line_points = [(320, 5), (320, 475)]
    classes_to_count = [0, 2]  # person and car classes for count

    # Init heatmap
    heatmap_obj = heatmap.Heatmap()
    heatmap_obj.set_args(colormap=cv2.COLORMAP_PARULA,
                        imw=w,
                        imh=h,
                        view_img=True,
                        shape="circle",
                        count_reg_pts=line_points)

    while cap.isOpened():
        success, im0 = cap.read()
        if success:
            tracks = model.track(im0, persist=True, show=False, classes=classes_to_count, verbose=True)

        im0 = heatmap_obj.generate_heatmap(im0, tracks)

    cap.release()
    cv2.destroyAllWindows()
    ```

---

??? abstract "Sources"

    - https://docs.ultralytics.com/

    - https://inside-machinelearning.com/yolov8/

    - https://datacorner.fr/yolo/

    - https://www.aranacorp.com/fr/creer-votre-banque-dimage-avec-python/

    - https://blent.ai/blog/a/detection-images-yolo-tensorflow

    - https://www.aqsone.com/blog/articlescientifique/comment-detecter-les-objets-plus-efficacement-avec-yolo/ 