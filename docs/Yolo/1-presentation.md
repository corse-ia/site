# Présentation de Yolo

[YOLO (You Only Look Once)](https://docs.ultralytics.com){ target="_blank" } est un des algorithmes de détection d’objets les plus populaires dans le domaine du Deep Learning. 

Le modèle présente plusieurs avantages par rapport aux systèmes basés sur des classificateurs. Il examine l'image entière au moment du test afin que ses prédictions soient éclairées par le contexte global de l'image. Il effectue également des prédictions avec une seule évaluation de réseau, contrairement à des systèmes comme R-CNN qui en nécessitent des milliers pour une seule image. Cela le rend extrêmement rapide, plus de 1 000 fois plus rapide que R-CNN et 100 fois plus rapide que Fast R-CNN. 

Les modèles de détection, de segmentation et de pose sont entraînés sur l'ensemble de données [COCO](https://github.com/ultralytics/ultralytics/blob/main/ultralytics/cfg/datasets/coco.yaml){ target="_blank" }, tandis que les modèles de classification sont entraînés sur l'ensemble de données [ImageNet](https://github.com/ultralytics/ultralytics/blob/main/ultralytics/cfg/datasets/ImageNet.yaml){ target="_blank" }. Il est bien-sûr possible d'entraîner un modèle avec un jeu de données personnalisé.


^^**Exemples d'application :**^^

| ![gif foot](https://github.com/RizwanMunawar/ultralytics/assets/62513924/7d320e1f-fc57-4d7f-a691-78ee579c3442) | ![gif personne](https://github.com/RizwanMunawar/ultralytics/assets/62513924/86437c4a-3227-4eee-90ef-9efb697bdb43) | ![Img vehicules](https://github.com/RizwanMunawar/ultralytics/assets/62513924/c8a0fd4a-d394-436d-8de3-d5b754755fc7){ width="315" } |
|:------------------------------:|:-----------------------------------------------------------:|:-------------------------------------------------------------------------:|
| Détection de joueurs           | Détection de chute                                          | Estimation de vitesse                                                     |


## Architecture

Le modèle est un réseau de neurones convolutif (CNN). L'architecture comporte 24 couches convolutives suivies de 2 couches entièrement connectées. L'alternance de couches convolutives 1 × 1 réduit l'espace des fonctionnalités des couches précédentes.  

<figure markdown>
  ![Image Yolo implementation](https://assets-global.website-files.com/5d7b77b063a9066d83e1209c/63c697fd4ef3d83d2e35a8c2_YOLO%20architecture-min.jpg){ width="" }
  <figcaption>Figure 1 : Le modèle</figcaption>
</figure>

Plusieurs versions du même modèle ont été proposées depuis la sortie initiale de YOLO en 2015, chacune s'appuyant sur et améliorant son prédécesseur. Les couches convolutives sont pré-entrainées sur la classification [ImageNet](https://www.image-net.org/index.php  "Ouvrir la page web"){ target="_blank" } à la moitié de la résolution (image d’entrée 224 × 224), puis la résolution est doublée pour la détection.


## Ressources sur deep learning appliqué à la reconnaissance d’images

- Pour savoir ce qu'est une convolution et à quoi ça sert : [Traitement d’images (partie 6: Filtres & Convolution)](https://datacorner.fr/image-processing-6/){ target="_blank" }

- pour tester les convolutions : [Image Kernels](https://setosa.io/ev/image-kernels/){ target="_blank" }

- Les convolutions dans un CNN : [Traitement d’images (partie 7: Les Réseaux de neurones à convolution – CNN)](https://datacorner.fr/image-processing-7/){ target="_blank" }

- Publication des auteurs de Yolo : [You Only Look Once: Unified, Real-Time Object Detection](https://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Redmon_You_Only_Look_CVPR_2016_paper.pdf){ target="_blank" }

- Article technique de vulgarisation sur Yolo : [YOLO: Algorithm for Object Detection Explained [+Examples]](https://www.v7labs.com/blog/yolo-object-detection){ target="_blank" } 

- Article de Devoteam : ["Mieux comprendre le deep learning appliqué à la reconnaissance d’images"](https://france.devoteam.com/paroles-dexperts/mieux-comprendre-le-deep-learning-applique-a-la-reconnaissance-dimages/){ target="_blank" }



