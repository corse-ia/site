# Entraînement de Yolo sur des données personnalisées

Former un modèle d'apprentissage profond consiste à lui fournir des données et à ajuster ses paramètres afin qu'il puisse faire des prédictions précises.

^^**Documentations à consulter :**^^

[Train](https://docs.ultralytics.com/fr/modes/train/){ target="_blank"  .md-button }  [Former des données personnalisées](https://docs.ultralytics.com/fr/yolov5/tutorials/train_custom_data/){ target="_blank" .md-button }  [Aperçu des ensembles de données](https://docs.ultralytics.com/fr/datasets/){ target="_blank" .md-button }


## 1 - Collecter des images

Le modèle de Yolo doit s'entraîner sur des images similaires à celles qu'il rencontrera en situation réelle. Idéalement, il faut recueillir une grande variété d'images à partir de la même configuration (caméra, angle, éclairage, etc.) que celle dans laquelle le projet sera déployé.

Voici les [recommandations de la team Yolo](https://docs.ultralytics.com/fr/yolov5/tutorials/tips_for_best_training_results/){ target="_blank"} :

- **Images par classe :** ≥ 1500 images par classe recommandées.

- **Instances par classe :** ≥ 10000 instances (objets étiquetés) par classe recommandées.

- **Variété d'images :** Doit être représentative de l'environnement déployé. Pour les cas d'utilisation dans le monde réel, nous recommandons des images prises à différents moments de la journée, à différentes saisons, par différents temps, sous différents éclairages, sous différents angles, à partir de différentes sources (téléchargées en ligne, collectées localement, différentes caméras), etc.

- **Images d'arrière-plan :** Les images d'arrière-plan sont des images sans objet qui sont ajoutées à un ensemble de données pour réduire les faux positifs (FP). Il est recommandé d'ajouter entre 0 et 10 % d'images d'arrière-plan pour réduire le nombre de faux positifs (COCO contient 1000 images d'arrière-plan à titre de référence, soit 1 % du total). Aucune étiquette n'est requise pour les images d'arrière-plan.


Voici des informations provenant de la [documentation : Paramètres de formation](https://docs.ultralytics.com/fr/yolov5/tutorials/tips_for_best_training_results/#training-settings){ target="_blank" } :

- **Taille de l'image :** COCO s'entraîne à la résolution native de `--img 640` Cependant, en raison du grand nombre de petits objets dans l'ensemble de données, il peut être avantageux de s'entraîner à des résolutions plus élevées, telles que `--img 1280`.

- **Taille du lot :** Utilise le plus grand `--batch-size` possible. Les lots de petite taille produisent de mauvaises statistiques sur la norme des lots et doivent être évités.

!!! tip "Astuce : Comment créer de images à partir d'une vidéo ?"

    Pour créer un grand nombre d'images d'entraînement à partir d'une vidéo, tournée de préférence en conditions réelles, il faut [extraire les images de la vidéo avec le logiciel ffmpeg](https://www.bannerbear.com/blog/how-to-extract-images-from-a-video-using-ffmpeg/){ target="_blank" }. Par exemple, 2 minutes de vidéo à 30 images par seconde produisent 3600 images.


## 2 - Créer des étiquettes

_Documentation : ["Créer des étiquettes"](https://docs.ultralytics.com/fr/yolov5/tutorials/train_custom_data/#22-create-labels){ target="_blank" }._

Une fois les images collectées, il faut annoter (ou étiqueter, ou encore labeliser) les objets d'intérêt afin de créer une base de données à partir de laquelle le modèle pourra apprendre. L'étiquetage consiste à identifer les formes à reconnaitres dans les images et à leur assigner une étiquette (un label). 

<figure markdown>
  ![Image Yolo implementation](https://uploads-ssl.webflow.com/5f6bc60e665f54545a1e52a5/6152a275ad4b4ac20cd2e21a_roboflow-annotate.gif){ width="" }
  <figcaption>Annotation avec Roboflow (Source: Ultralytics)</figcaption>
</figure>

Le logiciel [label-studio](https://labelstud.io/guide/){ target="_blank" } est une solution possible pour étiqueter les images. Il est gratuit et simple d'utilisation.

1. Installer le module Python [`label-studio`](https://labelstud.io/guide/install#Install-with-pip){ target="_blank" } : 

    ``` bash linenums="0"
    pip install label-studio
    ```

2. **Lancer le logiciel** à partir de la ligne de commande :

    ``` bash linenums="0"
    label-studio
    ```

    Puis cliquer sur le lien affiché dans le terminal pour accéder à l'interface web du logiciel.

3. **Créer les classes des objets à détecter** dans `settings` ([Documentation](https://labelstud.io/guide/setup){ target="_blank" }).

4. **Définir les annotations** pour toutes les images du jeu de données ([Documentation](https://labelstud.io/guide/manage_data){ target="_blank" }) en dessinant sur chaque image un cadre à la souris et en lui associant une étiquette. Voici les [recommandations de la team Yolo](https://docs.ultralytics.com/fr/yolov5/tutorials/tips_for_best_training_results/){ target="_blank"} :

    - **Cohérence des étiquettes :** Toutes les instances de toutes les classes dans toutes les images doivent être étiquetées. Un étiquetage partiel ne fonctionnera pas.
    
    - **Précision des étiquettes :** Les étiquettes doivent entourer étroitement chaque objet. Il ne doit pas y avoir d'espace entre un objet et sa boîte de délimitation. Aucun objet ne doit manquer d'étiquette.

5. **Exporter les annotations** au format Yolo. Une annotation est un fichier texte (exemple: `image_chat_42.txt`) qui contient pour chaque image les coordonnées des classes dans l'image.

    ``` title="Exemple de fichier d'annotation avec 3 classes sur une image" linenums="0"
    2 0.5164888457807955 0.5103998275676263 0.1290009699321047 0.3414161008729389
    0 0.35257032007759453 0.5422998167906024 0.13482056256062072 0.38107554693393664
    1 0.9592628516003882 0.5940295290440778 0.07371483996120264 0.12242698566655924
    ```


## 3 - Organiser l'ensemble des données

_Voir documentation ["Organiser les répertoires"](https://docs.ultralytics.com/fr/yolov5/tutorials/train_custom_data/#23-organize-directories){ target="_blank" }._

1. Dans le dossier racine du projet, il doit y avoir un dossier contenant un sous-dossier nommés `datasets/nom_du_dataset` et des sous-répertoires `train/` et `val/` contenant chacun d'eux, un sous-répertoire `images/` et `labels/`.

    ``` linenums="0"
    datasets/nom_du_dataset/
                ├── train/
                │   ├── images/
                │   └── labels/
                └── val/
                    ├── images/
                    └── labels/
    ```

    On dépose dans le dossier `train` les images destinées à l'entrainement de Yolo et dans le dossier `val` les images destinées aux tests pendant l'entrainement. Une bonne estimation du nombre d'images est d'environ 100% pour `train` et 80% pour `val`.  


2. Créer un fichier `nom_du_dataset.yaml` qui décrit l'arborescence du jeu de données d'entrainement et le nom des classes associées aux numéros de labels. ([Documentation](https://docs.ultralytics.com/fr/yolov5/tutorials/train_custom_data/#21-create-datasetyaml){ target="blank" }).

    ``` yaml title="nom_du_dataset.yaml" linenums="0"
    # Train/val/test sets as 1) dir: path/to/imgs, 2) file: path/to/imgs.txt, or 3) list: [path/to/imgs1, path/to/imgs2, ..]
    path: '../datasets/nom_du_dataset'  # dataset root dir
    train: 'train/'  # train images (relative to 'path')
    val:  'val/' # val images (relative to 'path')
    test:   # test images  (optional)

    # class names
    names:
      0: 'Marie'
      1: 'Michel'
      2: 'Pierre'
    ```

    !!! warning ""

        Attention les numéros des classes sont à vérifier dans les fichiers de configuration de label-studio.


## 3 - Choix du modèle

_Documentation : [Model-selection](https://docs.ultralytics.com/fr/yolov5/tutorials/tips_for_best_training_results/#model-selection){ target="_blank" }_

Le modèle va conditionner les performance de la détection. Voici les différents modèles pré-entrainés pour la détection :

<figure markdown>
  ![Image Yolo model](https://github.com/ultralytics/yolov5/releases/download/v1.0/model_comparison.png){ width="700" }
  <figcaption>Les différents modèles de Yolo (Source: Ultralytics)</figcaption>
</figure>

- Poids pré-entraînés. Recommandé pour les ensembles de données de taille petite à moyenne (c'est-à-dire COV, VisDrone, GlobalWheat) : `yolov8n.pt` à `yolov8x.pt`.

- Pars de zéro. Recommandé pour les grands ensembles de données (c.-à-d. COCO, Objets365, OIv6) : `yolov8n.yaml` à `yolov8x.yaml`.


## 4 - Entraînement du modèle

!!! tip "Astuce"

    - Utiliser une carte graphique NVidia compatible [Cuda](https://www.nvidia.com/fr-fr/technologies/cuda-x/){ target="_blank" } permet d'utiliser le GPU pour accélérer l'apprentissage (paramètre `device`).

    - Sur une machine avec beaucoup de mémoire vive le paramètre `cache=True` accélère l'apprentissage au détriment d'une plus grande occupation mémoire car le temps de chargement des images ne se fait plus depuis le disque dur mais depuis la RAM du système.


1. On créer un script Python pour entraîner le modèle. Le paramètre `name` est le nom du modèle choisi à l'étape précédente.

    Extraits de la documentation : [Paramètres de formation](https://docs.ultralytics.com/fr/yolov5/tutorials/tips_for_best_training_results/#training-settings){ target="_blank" } :

    - Époques. Commence avec 300 époques. Si l'adaptation est trop rapide, tu peux réduire le nombre d'époques. Si l'adaptation excessive ne se produit pas après 300 époques, entraîne-toi plus longtemps, c'est-à-dire 600, 1200, etc. époques.

    - Les meilleurs résultats d'inférence sont obtenus à la même résolution. `--img` que l'entraînement, c'est-à-dire que si tu t'entraînes à `--img 1280` tu dois aussi tester et détecter à `--img 1280`.


    ``` python title="yolo_training.py"
    from ultralytics import YOLO

    # Load the model.
    model = YOLO('yolov8n.yaml')  # build a new model from YAML
    #model = YOLO('yolov8n.pt')  # load a pretrained model (recommended for training)
    #model = YOLO('yolov8n.yaml').load('yolov8n.pt')  # build from YAML and transfer weights

    # Training :
    results = model.train(
        data='face_recognition.yaml',
        name='yolov8n_face',
        resume=False,  # Reprend un entraînement si True
        imgsz=1280,  # Défaut : img redim to 640
        epochs=100, 
        batch=-1,  # -1 = auto. Passer à 16 ou moins si pb mémoire GPU
        workers=8,  # Nbre de threads       
        device=cpu,  # Utilise le CPU (lent)
        #device=0,   # Utilise un GPU compatible Cuda
        cache=False,  # Réduit utilisation DD mais augment RAM 
    )
    ```

2. Lancer le script, l'entraînement est très long. 

    !!! bug "Le script plante"

        - Un plantage sans message d'erreur explicite peut survenir à cause d'un manque de RAM de l'OS. Il faut surveiller l'occupation mémoire avec le moniteur système et éventuellement rajouter de la RAM ou augmenter la taille du SWAP (ralentit l'entraînement). 

            - Diminuer la résolution des images `imgsz` permet de réduire l'occupation mémoire.

            - Passer le paramètre `cache=False` diminue l'occupation mémoire mais ralentit l'apprentissage.


        - En utilisant un GPU : Lorsque le paramètre `batch` (le nombre d'images à entraîner simultanément) est réglé sur `-1` (auto) la mémoire du GPU peut saturer et entraîner un plantage du script. Dans ce cas il faut chercher une valeur du batch qui sature pas la mémoire du GPU (la valeur est 16 par défaut, on peut passer à 12, 8, etc).


### 5 - Suivi de performance des entraînements

_Documentation : ["Enregistrement local"](https://docs.ultralytics.com/fr/yolov5/tutorials/train_custom_data/#local-logging){ target="_blank" }._

Le dossier  `runs/detect/nom_du_modele` (paramètre `name`) contient des fichiers images (jpg et png) qui permettent d'évaluer les performances de l'entrainement :

``` linenums="0"
yolov8n_face
├── confusion_matrix_normalized.png
├── confusion_matrix.png
├── F1_curve.png
...
├── train_batch2.jpg
├── val_batch0_labels.jpg
├── val_batch0_pred.jpg
```

<figure markdown>
  ![Image Yolo metrics](https://github.com/ultralytics/yolov5/releases/download/v1.0/results.png){ width="800" }
  <figcaption>Résultats de l'entraînement (Source: Ultralytics)</figcaption>
</figure>

D'une façon générale les `_loss` (perte) doivent baisser et les `metrics` doivent augmenter.


## 6 - Récupération du modèle entraîné

Les fichiers d'entrainements sons stockés dans le dossier `runs/detect/`. 

A chaque lancement du script d'entrainement, Yolo crée un sous-dossier dont le nom est définit dans le script d'entrainement par le paramètre `name` incrémenté (`runs/detect/detection_chats1`, `runs/detect/detection_chats2`, etc).

**Le modèle entrainé à utiliser dans les scripts de détection est le fichier `runs/detect/nom_du_modele/weights/best.pt`. Il est sauvegardé à la fin de chaque epoch.**

``` title="Exemple d'arborescence" linenums="0"
── runs
│   └── detect
│       ├── yolov8n_face
│       │   ├── args.yaml
│       │   ├── confusion_matrix_normalized.png
│       │   ├── confusion_matrix.png
...
│       │   ├── val_batch0_labels.jpg
│       │   ├── val_batch0_pred.jpg
│       │   └── weights
│       │       ├── best.pt
│       │       └── last.pt

```

Ainsi, dans un script de détection il suffira de spécifier le chemin vers le modèle qui vient d'être entraîné :

``` python title="Exemple" linenums="0"
model = YOLO('./runs/detect/yolov8n_face/weights/best.pt')
```

