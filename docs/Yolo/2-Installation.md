
# Installation de Yolo

Il est fortement conseillé d'installer les modules dans un [environnement virtuel Python](https://docs.python.org/fr/3/tutorial/venv.html){ target="_blank" }.


??? note "Connexion Internet derrière un proxy"

    Dans un lycée, la connexion Internet est réalisée à partir d'un proxy. Il faut le déclarer dans le système d'exploitation en tapant les deux lignes de code suivante dans un terminal :

    === ":simple-linux: Systèmes Linux"

        ``` bash linenums="0"
        export http_proxy="172.16.3.1:3128"
        export https_proxy="172.16.3.1:3128"
        ```

    === ":poop: Systèmes Windows"

        ``` bash linenums="0"
        set http_proxy="172.16.3.1:3128"
        set https_proxy="172.16.3.1:3128"
        ```


1. Installer le module Python [`ultralytics`](https://www.ultralytics.com/fr/){ target="_blank" } : 

    ``` bash linenums="0"
    pip install ultralytics
    ```

2. Installer le module Python [`Open CV`](https://opencv.org/){ target="_blank" } :

    ``` bash linenums="0"
    pip install opencv-python
    ```

3. Installer le module Python [`imutils`](https://github.com/PyImageSearch/imutils){ target="_blank" } :

    ``` bash linenums="0"
    pip install imutils
    ```