# Découverte de l’intelligence artificielle

_Auteur: Joseph-Antoine CESARI_

##Notion d’intelligence artificielle

L’intelligence artificielle est l’ensemble des théories et techniques qui permettent de faire réaliser par une machine des tâches que l’homme accomplit en utilisant son intelligence.

Ce terme apparait en 1956.