# Sommaire

## Classification d'objets avec Yolo

- [Présentation de Yolo](./Yolo/1-presentation.md)
- [Installation](./Yolo/2-Installation.md)
- [Détection avec le modèle pré entraîné](./Yolo/3-detection_modele_pre_entraine.md)
- [Entraînement sur des données personnalisées](./Yolo/4-entrainement_modele.md)



## Ressources
 
* [:material-language-python: Python Tutor](https://pythontutor.com/visualize.html#mode=edit){target="_blank"}



## Logiciels

* [:material-language-python: Bac à sable Python en ligne](https://console.basthon.fr){target="_blank"}

* [:material-download: Python 3 + idle3](https://www.python.org/downloads){target="_blank"}

